import re
import smtplib, ssl
import tkinter
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from tkinter import filedialog, LEFT
from tkinter.font import Font

port = 465  # For SSL

# Make a regular expression for validating an Email
regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

def check_email(email):
    # pass the regular expression and the string in search() method
    if re.search(regex, email):
        return True
    else:
        return False


def select_folder():
    global file_dir
    file_dir = filedialog.askopenfilename(parent=fenetre, initialdir="./", title='Selectionner un fichier')


def send_email():
    sender_email = entry_sender_email.get().strip()

    if check_email(sender_email):
        password = "projetpythonmail!1"
        receiver_email = entry_receiver_email.get().strip()

        if check_email(receiver_email):
            body = entry_message.get().strip()

            message = MIMEMultipart()

            message["From"] = sender_email
            message["To"] = receiver_email

            message.attach(MIMEText(body, "plain"))

            try:
                filename = file_dir
                with open(filename, "rb") as attachment:
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())

                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {filename}",
                )

                message.attach(part)
                text = message.as_string()
                # Create a secure SSL context
                context = ssl.create_default_context()

                with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
                    server.login(sender_email, password)
                    # Send email
                    server.sendmail(sender_email, receiver_email, text)
            except Exception:
                print()

        else:
            print("Adresse mail de destination non valide !")

    else:
        print("Votre adresse mail est non valide !")


# ===================================================================================================================
# ------------------------------------------- GESTION INTERFACE GRAPHIQUE -------------------------------------------
# ===================================================================================================================
fenetre = tkinter.Tk()
fenetre.configure(bg="#F0F0F0")
text = tkinter.Text(fenetre)
fontText = Font(family="Microsoft Sans Serif", size=10)
fenetre.geometry("900x500")
fenetre.title("Envoyer un mail")

text_label_sender_email = tkinter.StringVar()
text_label_sender_email.set("Saissez votre mail : ")
label_sender_email = tkinter.Label(fenetre, textvariable=text_label_sender_email, justify=LEFT, bd=10)
label_sender_email.grid(row=0, column=0, sticky=tkinter.W, columnspan=1)
label_sender_email.configure(font=fontText)

entry_sender_email = tkinter.Entry(fenetre, bd=2)
entry_sender_email.grid(row=0, column=1, sticky=tkinter.W, columnspan=1)

text_label_receiver_email = tkinter.StringVar()
text_label_receiver_email.set("Saissez l'adresse mail de destination : ")
label_receiver_email = tkinter.Label(fenetre, textvariable=text_label_receiver_email, justify=LEFT, bd=10)
label_receiver_email.grid(row=1, column=0, sticky=tkinter.W, columnspan=1)
label_receiver_email.configure(font=fontText)

entry_receiver_email = tkinter.Entry(fenetre, bd=2)
entry_receiver_email.grid(row=1, column=1, sticky=tkinter.W, columnspan=1)

text_label_message = tkinter.StringVar()
text_label_message.set("Saisissez votre message : ")
label_message = tkinter.Label(fenetre, textvariable=text_label_message, justify=LEFT, bd=10)
label_message.grid(row=4, column=0, sticky=tkinter.W, columnspan=1)
label_message.configure(font=fontText)

entry_message = tkinter.Entry(fenetre, bd=2, width=100)
entry_message.grid(row=5, column=0, sticky=tkinter.W, columnspan=1)

button_select_folder = tkinter.Button(fenetre, text="Séléctionner fichier", command=select_folder, bg="#E1E1E1", bd=1)
button_select_folder.grid(row=3, column=0, sticky=tkinter.W, padx=10, pady=20)
button_select_folder.configure(font=fontText)

send_button = tkinter.Button(fenetre, text="Envoyer le mail", command=send_email, bg="#E1E1E1", bd=1)
send_button.grid(row=0, column=2, sticky=tkinter.W, padx=20, pady=20)
send_button.configure(font=fontText)

fenetre.mainloop()
# ===================================================================================================================
# ----------------------------------------- FIN GESTION INTERFACE GRAPHIQUE -----------------------------------------
# ===================================================================================================================
